FROM debian:10

RUN apt-get update && apt-get upgrade -y && apt-get install -y python3 python3-pip libpq-dev python3-dev
WORKDIR /app
COPY ./django_blog/. .
RUN python3 -m pip install -r requirements.txt